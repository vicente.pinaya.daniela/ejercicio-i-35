
require('colors')
const fs = require('fs');

const crearSuma = (base = 2)=> { 

    let salidaS = '';
    titulo= `==================\nTabla del ${base}\n==================`.rainbow
    subtitulo = `\n==================\n`
    console.log(titulo);

    for(let i = 1; i<=10; i++){
      salidaS += `${base} + ${i} = ${base + i}\n`;
    }

    console.log(`${subtitulo}----SUMA-----${subtitulo}${salidaS}`.blue);

    fs.writeFile(`Tabla-suma-${base}.txt`, salidaS, (err) => {
      if(err) throw err;
      console.log(`Tabla-suma-${base}.txt creado`.magenta);
    });
};



const crearResta = (base = 2)=> { 

  let salidaR = '';
  titulo= `==================\nTabla del ${base}\n==================`.rainbow
  subtitulo = `\n==================\n`
  console.log(titulo);

  for(let i = 1; i<=10; i++){
    salidaR += `${base} - ${i} = ${base- i}\n`;
  }

  console.log(`${subtitulo}-----RESTA-----${subtitulo}${salidaR}`.green);

  fs.writeFile(`Tabla-resta-${base}.txt`, salidaR, (err) => {
    if(err) throw err;
    console.log(`Tabla-resta-${base}.txt creado`.magenta);
  });
};



const crearMulti = (base = 2)=> { 

  let salidaM = '';
  titulo= `==================\nTabla del ${base}\n==================`.rainbow
  subtitulo = `\n==================\n`
  console.log(titulo);

  for(let i = 1; i<=10; i++){
    salidaM += `${base} x ${i} = ${base* i}\n`;
  }

  console.log(`${subtitulo}  MULTIPLICACION   ${subtitulo}${salidaM}`.yellow);

  fs.writeFile(`Tabla-multiplicacion-${base}.txt`, salidaM, (err) => {
    if(err) throw err;
    console.log(`Tabla-multiplicacion-${base}.txt creado`.magenta);
  });
};


const crearDivision = (base = 2)=> { 

  let salidaD = '';
  titulo= `==================\nTabla del ${base}\n==================`.rainbow
  subtitulo = `\n==================\n`
  console.log(titulo);

  for(let i = 1; i<=10; i++){
    salidaD += `${base} / ${i} = ${base/ i}\n`;
  }

  console.log(`${subtitulo}DIVISION${subtitulo}${salidaD}`.cyan);

  fs.writeFile(`Tabla-division-${base}.txt`, salidaD, (err) => {
    if(err) throw err;
    console.log(`Tabla-division-${base}.txt creado`.magenta);
  });
};



//exportacion del archivo
module.exports = {
  generarArchivos: crearSuma, 
  generarResta: crearResta,
  generarMulti: crearMulti,
  generarDivision: crearDivision
};
