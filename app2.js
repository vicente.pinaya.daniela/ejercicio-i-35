
//importacion de archivos abajo
const { generarArchivos } = require('./helpers/operacionesYarg')
const { generarResta } = require('./helpers/operacionesYarg')
const { generarMulti } = require('./helpers/operacionesYarg')
const { generarDivision } = require('./helpers/operacionesYarg')

const argv = require('yargs').argv;

//console.log('base: yargs', argv.base);

generarArchivos(argv.base)
generarResta(argv.base)
generarMulti(argv.base)
generarDivision(argv.base)
// 35.- Recibir argumentos para sumar, restar, multiplicar y dividir del 1 al 100:
// • Sin yargs.
// • Con yargs.
// Realizar la escritura en un archivo txt, dentro de una carpeta llamada salida-operaciones que está en la
// raíz del proyecto.
// Subir al repositorio de GitLab.